<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller as Controller;


class APIBaseController extends Controller
{


    public function sendResponse($result, $message)
    {
        $systemInfo = [exec('hostname'), exec('arch'), exec('uname -sr'), 
                       exec('cat /etc/os-release | grep PRETTY_NAME')];
        $systemInfo[3] = substr($systemInfo[3], strpos($systemInfo[3], "=") + 1); 
        $systemInfo[3] = str_replace('"', '',$systemInfo[3]);
        //$systemInfo[3] = substr($systemInfo[3], 0, strpos($systemInfo[3], " "));
        $availableSpace = exec('df -h | grep -w "/"');
        $availableSpace = preg_replace('!\s+!', ' ', $availableSpace);
    	$response = [
            'systemInfo' => $systemInfo,
            'availableSpace' => $availableSpace,
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];


        return response()->json($response, 200);
    }


    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
}
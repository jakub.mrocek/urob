import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Navbar from './comp/navbar';
import Dashboard from './comp/dashboard';

class Index extends Component {
    constructor(props) {
        super(props);
        this.state = {
 
        };
    }

    render() {
        return(
        <div>
            <Navbar />
            <Dashboard />
            <blockquote className="blockquote">
                <p className="mb-0 text-center">Jakub Mrocek 2018</p>
                <a href="mailto:jakub.mrocek1990@gmail.com" className="center-link"> jakub.mrocek1990@gmail.com</a>
            </blockquote>
        </div>
        );
    }
}

ReactDOM.render(<Index />, document.getElementById('index'));
import React from 'react';
import {Pie} from 'react-chartjs-2';

const AvailableSpace = ( {data} ) => {

    const output = "" + data.availableSpace;
    const outputArray = output.split(' ');
    const chartNumbers = parseInt(outputArray[4]);

    const chart  = {
        datasets: [{
        backgroundColor: ["#ff3d00", "#2979ff"],
        data: [chartNumbers, 100-chartNumbers]
    }],

    labels: [
        'Used',
        'Free'
    ],

    options: {
        legend: {
            "display": false
        }
    }

};

    return(
        <div>
            <p className="card-text">
                <b>Disk:    </b>{outputArray[0]} <br />
                <b>Size: </b>{outputArray[1]} <br />
                <b>Used: </b>{outputArray[2]} <br />
                <b>Available: </b>{outputArray[3]} <br />
                <b>Used %: </b>{outputArray[4]} <br />
                <Pie data={chart} />
            </p>

        </div>
    );

};


export default AvailableSpace;
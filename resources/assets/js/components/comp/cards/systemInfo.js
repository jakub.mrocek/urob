import React from 'react';

const SystemInfo = ( {data} ) => {
    
    return(
        <div>
            <p className="card-text">
                <b>Hostname: </b>{data[0]} <br />
                <b>Architecture: </b>{data[1]} <br />
                <b>Kernel: </b>{data[2]} <br />
                <b>OS: </b>{data[3]} <br />
            </p>
        </div>
    );
};


export default SystemInfo;
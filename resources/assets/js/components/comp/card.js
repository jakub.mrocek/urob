import React from 'react';

import SystemInfo from './cards/systemInfo';
import AvailableSpace from './cards/availableSpace';

const Card = ({ cardName, data, styl }) => {
    let cardChosen;
    let cardType;
    switch (cardName) {
        case "System Info":
            cardChosen = <SystemInfo data={data}/>;
            cardType = "card mb-3 dashboard-card-shadow";
            break;
        
        case "Available Space":
            cardChosen = <AvailableSpace data={data}/>;
            cardType = "card mb-3 dashboard-card-shadow";
            break;
    
        default:
            cardChosen = <p> Unknown </p>;
            break;
    }
    return(
        <div className={cardType} style={styl}>
            
            <div className="card-body">
            <h5 className="card-title"><b>{cardName}</b></h5>
            {cardChosen}
            </div>
        </div>
    );
};

export default Card;
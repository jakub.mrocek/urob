import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Card from './card';

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            cardStyle: { width: '18rem' },
            data: [],
            systemInfo: []
        };
    }

    componentDidMount(){
        axios
          .get('http://localhost:8000/api/posts/')
          .then(({ data })=> {
              this.setState({ 
              data: data,
              systemInfo: data.systemInfo
            });
          })
          .catch((err)=> {})
      }

    render() {
        return(
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">Dashboard</h5>
                        <Card cardName="System Info" data={this.state.systemInfo} styl={this.state.cardStyle} />
                        <Card cardName="Available Space" data={this.state.data} styl={this.state.cardStyle} />
                        {console.log(this.state.data)}
                        {console.log(this.state.systemInfo[2])}
                </div>
            </div>
        );
    }
}

export default Dashboard;
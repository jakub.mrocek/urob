import React, { Component } from 'react';

const Navbar = () => {
    const vcolor = {
        color: '#9E9E9E'
    };
    return(
        <nav className="navbar navbar-dark bg-dark sticky-top navbar-urob">
            <a className="navbar-brand" href="#">
                <img src="https://cdn2.iconfinder.com/data/icons/ballicons-2-free/100/wrench-512.png" width="30" height="30" className="d-inline-block align-top" alt="" />
                 UROB <span style={vcolor}>v0.1   </span>
                <div className="btn-group" role="group" aria-label="Basic example">
                    <button type="button" className="btn btn-secondary active">Dashboard</button>
                    <button type="button" className="btn btn-secondary">Modules</button>
                </div>
                
            </a>
        </nav>
    );
};

export default Navbar;

